// Package plugins includes the plugins we want to load
package plugins

import (
	"gitlab.com/diycoder/go-micro/config/cmd"

	// import specific plugins
	ckStore "gitlab.com/diycoder/go-micro/store/cockroach"
	fileStore "gitlab.com/diycoder/go-micro/store/file"
	memStore "gitlab.com/diycoder/go-micro/store/memory"
	// we only use CF internally for certs
	cfStore "gitlab.com/diycoder/micro/internal/plugins/store/cloudflare"
)

func init() {
	// TODO: make it so we only have to import them
	cmd.DefaultStores["cloudflare"] = cfStore.NewStore
	cmd.DefaultStores["cockroach"] = ckStore.NewStore
	cmd.DefaultStores["file"] = fileStore.NewStore
	cmd.DefaultStores["memory"] = memStore.NewStore
}
