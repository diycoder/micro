package main

import (
	"gitlab.com/diycoder/micro/cmd"
)

func main() {
	cmd.Init()
}
