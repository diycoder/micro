module gitlab.com/diycoder/micro

go 1.16

require (
	github.com/aws/aws-sdk-go v1.40.9
	github.com/boltdb/bolt v1.3.1
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/cloudflare/cloudflare-go v0.10.9
	github.com/dustin/go-humanize v1.0.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-acme/lego/v3 v3.9.0
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.3.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/micro/cli/v2 v2.1.2
	github.com/miekg/dns v1.1.43
	github.com/nats-io/nats-server/v2 v2.4.0 // indirect
	github.com/netdata/go-orchestrator v0.0.0-20190905093727-c793edba0e8f
	github.com/olekukonko/tablewriter v0.0.5
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/serenize/snaker v0.0.0-20201027110005-a7ad2135616e
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	github.com/xlab/treeprint v1.1.0
	gitlab.com/diycoder/go-micro v0.0.3-0.20210922145617-62835d643ffa
	gitlab.com/diycoder/tech-lab v0.0.2
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	golang.org/x/net v0.0.0-20210726213435-c6fcb2dbf985
	golang.org/x/tools v0.1.5
	google.golang.org/genproto v0.0.0-20210726200206-e7812ac95cc0
	google.golang.org/grpc v1.39.0
)
