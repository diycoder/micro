FROM alpine:latest as build
RUN apk --no-cache add make git go gcc libtool musl-dev tzdata

# Configure Go
ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV PATH /go/bin:$PATH
ENV GO111MODULE=on
ENV GOPROXY=https://goproxy.cn,direct

RUN mkdir -p ${GOPATH}/src ${GOPATH}/bin

COPY . /
RUN make

# 运行：使用scratch作为基础镜像
FROM alpine:3.14 as prod

# 在build阶段复制时区到
COPY --from=build /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
# 在build阶段复制可执行的go二进制文件app
COPY --from=build micro /data/bin/

WORKDIR /data
